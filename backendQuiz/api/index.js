const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')

const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql')
const connection = mysql.createConnection({
    host : 'localhost',
    user : 'buffet_admin',
    password : 'buffet_admin',
    database : 'BuffetSystem'
})

connection.connect()

const express = require('express')
const app = express()
const port = 4000

function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err, user) => {
        if (err) { return res.sendStatus(403) }
        else {
            req.user = user
            next()
        }
    })
}

app.post("/login", (req, res) => {
    let username = req.query.username
    let user_password = req.query.password
    let query = `SELECT * FROM Buffet WHERE Username = '${username}' `
    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                         "status" : "400",
                         "message" : "Error querying from running db"
                     })
         }else {
             let db_password = rows[0].Password
             bcrypt.compare(user_password, db_password, (err, result) => {
                if (result) {
                    let payload = {
                        "username" : rows[0].Username,
                        "user_id" : rows[0].BuffetID,
                        "IsAdmin" : rows[0].IsAdmin
                    }
                    console.log(payload)
                    let token = jwt.sign(payload, TOKEN_SECRET, { expiresIn : '1d' } )
                    res.send(token)
                }else { res.send("Invalid username / password") }
             }) 
         }
    })
})

app.post("/register_buffet", authenticateToken, (req, res) => {

    let buffet_name = req.query.buffet_name
    let buffet_surname = req.query.buffet_surname
    let buffet_people = req.query.buffet_people
    let buffet_username = req.query.buffet_username
    let buffet_password = req.query.buffet_password

    bcrypt.hash(buffet_password, SALT_ROUNDS, (err, hash) => {
        let query = ` INSERT INTO Buffet
                    (BuffetName, BuffetSurname, BuffetPeople, Username, Password, IsAdmin) 
                    VALUES ( '${buffet_name}','${buffet_surname}','${buffet_people}',
                             '${buffet_username}', '${hash}', false)`                    
        console.log(query)

        connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
           res.json({
                        "status" : "400",
                        "message" : "Error inserting data db"
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "Adding new user succesful"
            })
        }
        });
    })
});

/*SELECT*/
app.get("/list_registrations", authenticateToken, (req, res) => {
    let query = `
        SELECT Buffet.BuffetID, Buffet.BuffetName, Buffet.BuffetSurname, 
               Registration.RegistrationTime
   
               FROM Buffet, Registration
               WHERE (Registration.BuffetID = Buffet.BuffetID);`;

    connection.query( query, (err, rows) => {
        if (err) {
          console.log(err)
           res.json({
                        "status" : "400",
                        "message" : "Error querying from running db"
                    })
        }else {
            res.json(rows)
        }
    });
})


/*INSERT INTO*/
app.post("/add_buffet", (req, res) => {

    
    let buffet_name = req.query.buffet_name
    let buffet_surname = req.query.buffet_surname   
    let buffet_people = req.query.buffet_people
    
    let query = ` INSERT INTO Buffet
                    (BuffetName, BuffetSurname, BuffetPeople) 
                    VALUES ( '${buffet_name}','${buffet_surname}', '${buffet_people}')`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
           res.json({
                        "status" : "400",
                        "message" : "Error inserting data db"
                    })
        }else {
            res.json({
                        "status" : "200",
                        "message" : "Adding event succesful"
            })
        }
    });

})


/*UPDATE*/
app.post("/update_buffet", (req, res) => {

    let buffet_id = req.query.buffet_id
    let buffet_name = req.query.buffet_name
    let buffet_surname = req.query.buffet_surname
    let buffet_people = req.query.buffet_people
    let query = ` UPDATE Buffet SET
                    BuffetName = '${buffet_name}',
                    BuffetSurname = '${buffet_surname}',
                    BuffetPeople = '${buffet_people}'
                    WHERE BuffetID = '${buffet_id}'`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
           res.json({
                        "status" : "400",
                        "message" : "Error updating record"
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "Updating buffet succesful"
            })
        }
    });
})

/*DELETE*/
app.post("/delete_buffet", (req, res) => {

    let buffet_id = req.query.buffet_id

    let query = ` DELETE FROM Buffet WHERE BuffetID = ${buffet_id}`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
           res.json({
                        "status" : "400",
                        "message" : "Error deleting record"
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "Deleting record success"
            })
        }
    });
})

app.listen(port, () => {
    console.log(` Now starting Running System Backend ${port} `)
})